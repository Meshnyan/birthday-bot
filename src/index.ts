import 'reflect-metadata'
import Telegraf, { Extra, Markup, SceneContextMessageUpdate } from 'telegraf'
import {BaseScene, Stage, session} from 'telegraf'
import config from '../config.json'
import { dbConnection } from './db/connection'
import { getUserRepository } from './db/user.repository'
import { getTodoRepository } from './db/todo.repository'
import { getJokeTimeRepository } from './db/jokeTime.repository'
import {Status, Todo} from './entities/todo'
import {CronJob} from 'cron'
import * as Utils from './utils'
import { Repository } from 'typeorm'
import { User } from './entities/user'
import { JokeTime, Delay } from './entities/jokeTime'

dbConnection.then(
  connection => { 
    const userRepo = getUserRepository(connection)
    const todoRepo = getTodoRepository(connection)
    const jokeTimeRepo = getJokeTimeRepository(connection)

///Инициализация бота
    const bot = new Telegraf(config.token)
    
///Настройка сцен
    const addScene = new BaseScene('addScene')
    addScene.enter((ctx) => ctx.reply('Что добавить?', Extra.HTML().markup((m) =>
      m.inlineKeyboard([
        m.callbackButton('Завершить добавление', 'leaveScene'),
      ])
    )))
    addScene.leave((ctx) => ctx.reply('Изменения сохранены'))
    addScene.on('text', (ctx) => {
      Utils.addTodo(ctx, repos);
      ctx.reply('Что-то еще?', Extra.HTML().markup((m) =>
          m.inlineKeyboard([
          m.callbackButton('Завершить добавление', 'leaveScene')
        ])
      ));
    });

    const editScene = new BaseScene('editScene');
    editScene.enter(async (ctx) => {
      const todos = await Utils.renderTodo(ctx, repos, "EDIT")
      return ctx.replyWithHTML('Нажми на хэш для выбора позиции: \n\n' + todos, Extra.HTML().markup((m) =>
        m.inlineKeyboard([
            m.callbackButton('Выйти из редактирования', 'leaveScene'),
        ])) 
      )
    });
    editScene.leave((ctx) => ctx.reply('Редактирование завершено'));
    editScene.on('text', async (ctx) => {
      const res = await Utils.editTodo(ctx, repos)

      if (res.text) {
        ctx.replyWithHTML(`<b>Позиция для редактирования:</b>\n${res.text}\n<b>Статус: </b>${res.status}`,
          Extra.HTML().markup((m) =>
            m.inlineKeyboard([
              [
                  m.callbackButton('Изменить текст', 'editTextEnter'),
                  m.callbackButton('Изменить статус', 'editTodoStatus')
              ],
              [
                  m.callbackButton('Удалить', 'editTodoStatusDeleted'),
                  m.callbackButton('Завершить редактирование', 'leaveScene')
              ]
            ])
          )
        )
      }
      else {
        ctx.replyWithHTML("Позиция не найдена", Extra.HTML().markup((m) =>
            m.inlineKeyboard([
              m.callbackButton('Завершить редактирование', 'leaveScene')
            ])
          )
        )
      }
    })

    const editTextScene = new BaseScene('editTextScene');
    editTextScene.enter((ctx) => ctx.reply('Отправьте другой текст', Extra.HTML().markup((m) =>
      m.inlineKeyboard([
        m.callbackButton('Завершить редактирование', 'leaveScene'),
      ])
    )));
    editTextScene.leave((ctx) => ctx.reply('Редактирование завершено'));
    editTextScene.on('text', (ctx) => {
      Utils.editText(ctx, repos)
      ctx.reply('Текст отредактирован', Extra.HTML().markup((m) =>
        m.inlineKeyboard([
          m.callbackButton('Завершить редактирование', 'leaveScene'),
        ]))
      );
    });

    const stage = new Stage([addScene, editScene, editTextScene])
    
    bot.use(session());
    bot.use(stage.middleware());

///Переменные
    const editStatusKeyboard = (m) => {
      return (
        m.inlineKeyboard([
          m.callbackButton('Завершить редактирование', 'leaveScene'),
        ])
      );
    }

    const repos: {
      userRepo: Repository<User>;
      todoRepo: Repository<Todo>;
      jokeTimeRepo: Repository<JokeTime>;
    } = {
      userRepo: userRepo,
      todoRepo: todoRepo,
      jokeTimeRepo: jokeTimeRepo
    }
    
///cron job
    const jokeTime = new CronJob('0 */1 * * * *', async () => {
      console.log('jokeTime')
      const jokeList = await Utils.jokeList(repos);

      if(!jokeList.users)
        return

      jokeList.users.forEach((user) => {
        bot.telegram.sendMessage(user, jokeList.joke)
      })
    })

    jokeTime.start();

///Логика бота  
    bot.start((ctx) => {
      Utils.entryPoint(ctx, repos);
      ctx.reply("добрый день", Markup
      .keyboard(['/show', '/jokeSettings'])
      .resize()
      .extra()
      );
    });

    bot.command('show', async (ctx) => {
      const todos = await Utils.renderTodo(ctx, repos);
      return ctx.replyWithHTML(todos, Extra.HTML().markup((m) =>
      m.inlineKeyboard([
        m.callbackButton('Add', 'addEnter'),
        m.callbackButton('Edit', 'editEnter'),
      ])) 
      );
    });

    bot.command('test', (ctx) => {
      Utils.checkSubscribersForJoke(repos)
    })

    bot.command('jokeSettings', async (ctx) => {
      const jokeConfig = await Utils.getJokeCurrentConfig(ctx.update.message.from.id, repos);
      
      if(jokeConfig.isSubscriber) {
        const replyForSubscribers = `Статус:\n
          Вы подписаны на сервис Joke Time\n
          Шутки приходят не раньше, чем через ${jokeConfig.jokeDelay} минут${jokeConfig.jokeDelay == 1 ? 'у':''}\n
          Последняя шутка прозвучала ${jokeConfig.lastJokeTime}`;
        
        ctx.reply(replyForSubscribers, Extra.HTML().markup((m) =>
          m.inlineKeyboard([
            [m.callbackButton('Запили преколяс', 'joke')],
            [m.callbackButton('Выбрать шуткоинтервал', 'jokeDelayOptions')],
            [m.callbackButton('Отписаться', 'jokeSubscribeStatusSwitch')],
          ])
        ));
      }
      else {
        const replyForOther = `Статус:\n
          Вы не подписаны на сервис Joke Time\n
          Чтобы подписаться, воспользуйтесь формой ниже`;

          ctx.reply(replyForOther, Extra.HTML().markup((m) =>
          m.inlineKeyboard([
            m.callbackButton('Запили преколяс', 'joke'),
            m.callbackButton('Подписаться', 'jokeSubscribeStatusSwitch'),
          ])
        ));
      }
      
    })
    
    bot.action('editTodoStatus', ctx => ctx.reply('Статус:', Extra.HTML().markup((m) =>
      m.inlineKeyboard([
        m.callbackButton('В работе', 'editTodoStatusActive'),
        m.callbackButton('Сделано', 'editTodoStatusChecked'),
      ])
    )));
    
    bot.action('editTodoStatusActive', ctx => {
      Utils.editTodoStatus(ctx, Status.ACTIVE, repos)
      ctx.reply('Статус изменен', Extra.HTML().markup((m) => editStatusKeyboard(m)));
    })

    bot.action('editTodoStatusChecked', ctx => {
      Utils.editTodoStatus(ctx, Status.CHECKED, repos)
      ctx.reply('Статус изменен', Extra.HTML().markup((m) => editStatusKeyboard(m)));
    })

    bot.action('editTodoStatusDeleted', ctx => {
      Utils.editTodoStatus(ctx, Status.DELETED, repos)
      ctx.reply('Статус изменен', Extra.HTML().markup((m) => editStatusKeyboard(m)));
    })

    bot.action('addEnter', (ctx: SceneContextMessageUpdate) => ctx.scene.enter('addScene'))
    bot.action('editEnter', (ctx: SceneContextMessageUpdate) => ctx.scene.enter('editScene'))
    bot.action('editTextEnter', (ctx: SceneContextMessageUpdate) => ctx.scene.enter('editTextScene'))
    bot.action('leaveScene', (ctx: SceneContextMessageUpdate) => ctx.scene.leave()) 
    bot.action('editSceneLeave', (ctx: SceneContextMessageUpdate) => ctx.scene.leave()) 

    bot.action('joke', async (ctx) => {bot.telegram.sendMessage(ctx.update.callback_query.from.id, await Utils.fetchJoke())})
    bot.action('jokeDelayOptions', (ctx) => ctx.reply('Выбери интервал:', Extra.HTML().markup((m) =>
      m.inlineKeyboard([
        m.callbackButton('1 мин', 'editJokeDelayOne'),
        m.callbackButton('5 мин', 'editJokeDelayFive'),
        m.callbackButton('10 мин', 'editJokeDelayTen'),
      ])
    )));
    bot.action('jokeSubscribeStatusSwitch', async (ctx) => {
      const isSubscriber = await Utils.switchSubscribeStatus(ctx, repos)
      ctx.reply(`${isSubscriber ? 'Вы отписаны' : 'Вы подписаны'}`)
    })

    bot.action('editJokeDelayOne', ctx => {
      Utils.editJokeDelay(ctx, Delay.ONE, repos)
      ctx.reply('Готово, шутки будут прилетать не чаще, чем через 1 минуту');
    })
    bot.action('editJokeDelayFive', ctx => {
      Utils.editJokeDelay(ctx, Delay.FIVE, repos)
      ctx.reply('Готово, шутки будут прилетать не чаще, чем через 5 минут');
    })
    bot.action('editJokeDelayTen', ctx => {
      Utils.editJokeDelay(ctx, Delay.TEN, repos)
      ctx.reply('Готово, шутки будут прилетать не чаще, чем через 10 минут');
    })

    bot.hears('хуй', (ctx) => {ctx.replyWithPhoto('https://sun9-33.userapi.com/c854532/v854532676/1e8f8f/i4AP1kd2gaU.jpg')})
    bot.hears('Хуй', (ctx) => {ctx.replyWithPhoto('https://sun9-33.userapi.com/c854532/v854532676/1e8f8f/i4AP1kd2gaU.jpg')})

    bot.launch().then(() => {
      console.log('The Bot is ready now')
    })
  }
)