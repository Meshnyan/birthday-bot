import { Connection } from 'typeorm'
import { JokeTime } from '../entities/jokeTime'

export const getJokeTimeRepository = (connection: Connection) => connection.getRepository(JokeTime)
