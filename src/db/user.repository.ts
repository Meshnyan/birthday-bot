import { Connection, getConnection } from 'typeorm'
import { User } from '../entities/user'

export const getUserRepository = (connection: Connection) => connection.getRepository(User)