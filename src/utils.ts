import {Status, Todo} from './entities/todo'
import fetch from 'node-fetch'
import iconv from 'iconv-lite'
import { Repository, In } from 'typeorm'
import { User } from './entities/user'
import { JokeTime } from './entities/jokeTime'

class Storage {
  private store: {
    [key: string]: any
  }

  constructor() {
    this.store = {}
  }

  set(path, value) {
    this.store[path] = value
  }

  get(path) {
    return this.store[path]
  }
}

const store = new Storage();


const checkFieldIsNotExist = (user) => {
  if (user) {
    return false
  }
  console.log('field does not exist')
  return true
}


const findUser = async (ctx, repos: {
  userRepo: Repository<User>;
  todoRepo: Repository<Todo>;
  jokeTimeRepo: Repository<JokeTime>;
}) => {
  
  if (!(ctx.update.message || ctx.update.callback_query))
  return

  let user = await repos.userRepo.findOne({
    where: {
      telegramId: (ctx.update.message ? ctx.update.message.from.id : ctx.update.callback_query.from.id)
    }
  })

  if(checkFieldIsNotExist(user)){
    user = await dbWorker(repos.userRepo, {telegramId: ctx.update.message.from.id, username: ctx.update.message.from.username})
  }

  const a = await repos.jokeTimeRepo.findOne({
    where:{
      user
    }
  })

  if(checkFieldIsNotExist(a)) {
    dbWorker(repos.jokeTimeRepo, {user, lastTodoUpdate: new Date()})
    return user
  }

  a.lastTodoUpdate = new Date()
  repos.jokeTimeRepo.save(a)

  return user;
}


const dbWorker = async (repo, data: object) => {
  const a = repo.create(data);
  return await repo.save(a);
}


const todoStringify = (todos, flag = '') => {
  let res = 'На данный час времени нет записей в списке.';
  if (todos.length) {
      res = '';
      res = todos.reduce((acc, todo) => {
          switch(todo.status) {
              case Status.ACTIVE:
              return (`${acc}${flag ? ('/' + todo.id.slice(0,8) + ' ') : ''}<b>${todo.text}</b>\n`)
              case Status.CHECKED:
              return (`${acc}${flag ? ('/' + todo.id.slice(0,8) + ' ') : ''}<s>${todo.text}</s>\n`)
              case Status.DELETED:
              return acc
              default:
              return acc
          }
      }, res)
  }
  return res
}


export const entryPoint = async (ctx, repos) => {

  let user = await findUser(ctx, repos)

  if(checkFieldIsNotExist(user))
    return
}


export const renderTodo = async (ctx, repos, flag = '') => {
  const user = await findUser(ctx, repos);

  if(checkFieldIsNotExist(user))
    return
   
  const todos = await repos.todoRepo.find({
    where: {
      user,
    }
  });
  
  return todoStringify(todos, flag);
}


export const addTodo = async (ctx, repos) => {
  const user = await findUser(ctx, repos);
  
  if(checkFieldIsNotExist(user))
    return
   
  dbWorker(repos.todoRepo, {user, text: ctx.message.text});
}
  

export const editTodoStatus = async(ctx, flag, repos) => {
  let user = await findUser(ctx, repos);
  
  if(checkFieldIsNotExist(user))
    return
   
  dbWorker(repos.todoRepo, {user, status: flag, id: store.get('todo').id});
}
  

export const editText = async(ctx, repos) => {
  let user = await findUser(ctx, repos);
  
  if(checkFieldIsNotExist(user))
    return
   
  dbWorker(repos.todoRepo, {user, text: ctx.message.text, id: store.get('todo').id});
}
  

export const editTodo = async (ctx, repos) => {
  const user = await findUser(ctx, repos);
  
  if(checkFieldIsNotExist(user))
    return

  const todos = await repos.todoRepo.find({
    where: {
      user,
    }
  });

  const hash = ctx.message.text.slice(1);
  let res = {
    text: 'На данный час времени нет записей в списке.',
    id: '',
    status: ''
  }

  if(todos.length) {
      res.text = '';
      todos.forEach(todo => {
      if (todo.id.slice(0,8) == hash) {
          res.text = todo.text;
          res.id = todo.id;
          res.status = todo.status;
          return res;
      }
    });
  }

  store.set('todo', res)

  return res
}


export const jokeList = async (repos) => {
  const jokeList = {
    joke: await fetchJoke(),
    users: await checkSubscribersForJoke(repos)
  }
  return jokeList;
}


export const fetchJoke = async () => {
  const res = await fetch('http://rzhunemogu.ru/RandJSON.aspx?CType=11')
  const buffer = await res.buffer()
  const transformedText = iconv.encode(iconv.decode(buffer, 'win1251'),'utf-8').toString()
  return transformedText.match(/{"content":"(.*)"}/s)[1]
};


export const checkSubscribersForJoke = async (repos) => {
  const users = await repos.userRepo.find({
    relations: ['jokeTime'],
  });

  const date = new Date();
  const delay = 60*1000;

  const usersForJoke = users.filter(user => Boolean(user.jokeTime)).filter(user => {
    return (
      ((date.valueOf() - user.jokeTime.lastTodoUpdate.valueOf()) >= delay)
      && 
      (user.jokeTime.isSubscriber)
      &&
      ((date.valueOf() - user.jokeTime.lastJokeTime.valueOf()) >= (user.jokeTime.jokeDelay * delay))
    )
  }).map(user => user.telegramId)

  if (!usersForJoke.length) 
    return

  const usersByTelegramId = await repos.userRepo.find({
    select: [ 'id' ],
    where: {
      telegramId: In(usersForJoke)
    }
  })

  if(!usersByTelegramId.length)
    return

  const a = await repos.jokeTimeRepo.find({
    where:{
      userId: In(usersByTelegramId.map(user => user.id))
    }
  })

  a.forEach((_, index) => {
    a[index].lastJokeTime = new Date()
  })

  repos.jokeTimeRepo.save(a)
  
  return usersForJoke;
}


export const getJokeCurrentConfig = async (telegramId, repos) => {
  const userByTelegramId = await repos.userRepo.findOne({
    select: [ 'id' ],
    where: {
      telegramId: telegramId
    }
  })

  if(!userByTelegramId)
    return

  const jokeConfig = await repos.jokeTimeRepo.findOne({
    where:{
      user: userByTelegramId
    }
  })

  return jokeConfig
}


export const switchSubscribeStatus = async (ctx, repos) => {
  const user = await findUser(ctx, repos);

  if(checkFieldIsNotExist(user))
    return

  
  const jokeConfig = await repos.jokeTimeRepo.findOne({
    where:{
      user
    }
  })

  if(checkFieldIsNotExist(jokeConfig))
  return

  const isSubscriber = jokeConfig.isSubscriber
  console.log("Подписан: ",isSubscriber)
  jokeConfig.isSubscriber = !isSubscriber
  console.log("Подписан: ",isSubscriber)
  repos.jokeTimeRepo.save(jokeConfig)

  return isSubscriber
}


export const editJokeDelay = async (ctx, flag, repos) => {
  let user = await findUser(ctx, repos);

  if(checkFieldIsNotExist(user))
    return

  const jokeConfig = await repos.jokeTimeRepo.findOne({
    where:{
      user
    }
  })

  if(checkFieldIsNotExist(jokeConfig))
  return
  jokeConfig.jokeDelay = flag
  
  repos.jokeTimeRepo.save(jokeConfig)
}