import { 
    Entity, Column, PrimaryGeneratedColumn, OneToMany, OneToOne, CreateDateColumn, UpdateDateColumn, Index
} from "typeorm";
import { Todo } from './todo';
import { JokeTime } from './jokeTime';

@Entity()
export class User {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Index()
    @Column({
        unique: true
    })
    telegramId: number;
    
    @Column({
        nullable: true
    })  
    username: string;

    @CreateDateColumn()
    createdDate: Date;

    @UpdateDateColumn()
    updatedDate: Date;

    @OneToMany(type => Todo, t => t.user)
    todos: Todo[];

    @OneToOne(type => JokeTime, t => t.user)
    jokeTime: JokeTime;
    
}