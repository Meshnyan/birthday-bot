import { 
    Entity, Column, OneToOne, PrimaryGeneratedColumn, JoinColumn
} from "typeorm";
import { User } from './user';

export enum Delay {
    ONE = 1,
    FIVE = 5,
    TEN = 10,
}

@Entity()
export class JokeTime {
    @PrimaryGeneratedColumn('uuid') 
    id: string;

    @Column({
        default: new Date(0)
    })
    lastTodoUpdate: Date;

    @Column({
        type: 'enum',
        enum: Delay,
        default: Delay.FIVE
    })  
    jokeDelay: Delay;

    @Column({
        default: new Date(0)
    })
    lastJokeTime: Date;
    
    @Column({
        default: true
    })
    isSubscriber: boolean;

    @OneToOne(() => User, user => user.jokeTime)
    @JoinColumn()
    user: User;
}