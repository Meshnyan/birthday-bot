import { 
    Entity, Column, ManyToOne, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn
} from "typeorm";
import { User } from './user';

export enum Status {
    ACTIVE = 'active',
    CHECKED = 'checked',
    DELETED = 'deleted',
}

@Entity()
export class Todo {
    @PrimaryGeneratedColumn('uuid') 
    id: string;

    @Column()  
    text: string;

    @Column({
        type: 'enum',
        enum: Status,
        default: Status.ACTIVE
    })  
    status: Status;

    @CreateDateColumn()
    createdDate: Date;

    @UpdateDateColumn()
    updatedDate: Date;

    @ManyToOne(() => User, t => t.todos)
    user: User;
}